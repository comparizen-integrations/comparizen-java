package com.comparizen.client.model;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class DiffConfig {
	public static final DiffConfig VISIBLE_FOR_HUMAN_EYES = DiffConfig.builder().perPixelThreshold(1.0).totalDiffPercentageThreshold(0.0).build();
	public static final DiffConfig STRICT = DiffConfig.builder().perPixelThreshold(0.0).totalDiffPercentageThreshold(0.0).build();

	/**
	 * When comparing 2 images, this value determines how much a single pixel must be different for it to be marked as 'not matching'. The value here can be between 0 and 100, where a value between 0-1 is not visible by the human eye, 1- 2 is perceptible through close observation, 2-10 perceptible at a glance, 11 - 49 colors are more similar than opposite and 100 is the exact color opposite (ie. black and white). Leaving this value empty will cause the comparison to use defaults set in the project's configuration.
	 */
	Double perPixelThreshold;

	/**
	 * When comparing 2 images, this value determines what minimum percent of the image's total amount of pixels should be marked as different before marking the images as 'not matching'. A value of 0 means: one single pixel-difference will mark the images as 'not matching'. A value of 50 will cause images to be 'not matching' only if more than 50% of the image's pixels are marked as different. Leaving this value empty will cause the comparison to use defaults set in the project's configuration.
	 */
	Double totalDiffPercentageThreshold;

}
