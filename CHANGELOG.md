# Comparizen-java-client changelog

### 0.0.11

- Added support for exclude regions
- Minor dependency upgrades

### 0.0.10

- Minor dependency upgrades

### 0.0.9

- Fixed issue where warning message `Could not find class: java.time.ZoneRules` would be logged in `System.out`.

### 0.0.8

- Improved error handling and added more clarifying error messages

### 0.0.7

- Added more elaborate README.md, including a 'Getting Started' chapter
- Upgraded various project dependencies

### 0.0.6

- Upgraded various project dependencies

### 0.0.5

- Added support for Java 8 

### 0.0.4

- Added support for comparison diff precision configuration

### 0.0.3

- Added support for environment properties
- Added bitbucket support

### 0.0.2

- Added support for adding name to test run

### 0.0.1

- Initial release

