# Comparizen Java Client

A Java client library for [Comparizen](https://comparizen.com).

## Getting started

Before using this library, make sure you're familiar with Comparizen's core concepts.
Consult the documentation [here](https://app.comparizen.com/docs).

### 1. Add dependency
First, add the library to your Gradle/Maven's project dependencies:

Gradle:
```gradle
implementation 'com.comparizen:comparizen-java-client:0.0.11'
```

Maven:
```maven
<dependency>
    <groupId>com.comparizen</groupId>
    <artifactId>comparizen-java-client</artifactId>
    <version>0.0.11</version>
</dependency>
```

### 2. Initialize the client

Initialize the client:

```java
var client = ComparizenClient.builder()
        .apiKey(<your api key>)
        .build();
```
You can find your project's API key in your Comparizen project's settings.

Note: make sure not to commit this API key to any code repository. It is recommended
to pass the API key using a environment variable (eg `System.getEnv('MY_COMPARIZEN_API_KEY')`)
or use a configuration file like [dot-env](https://github.com/uzzu/dotenv-gradle).

### 3. Send screenshots to Comparizen

First, create a test-run and obtain the test-run's ID.
```java
String testRunId = client.createTestRun(<your project's ID>, "my testrun name");
```
Note: you can find your Comparizen project's ID in the project's settings.

Then, start uploading screenshots to that testrun:
```java
client.createComparison(testRunId, "some screenshot name", Paths.get(ClassLoader.getSystemResource("screenshot.png").toURI()));
```

After all screenshots have been sent, finalize the test-run:
```java
client.finalizeTestRun(testRunId);
```

[A full example can be found here](src/test/java/integrationtests/ComparizenClientIT.java). 




