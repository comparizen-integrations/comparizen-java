package com.comparizen.client.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TestRunStatusResponse {
    private final TestRunStatus testRunStatus;
    private final String url;
}
