package com.comparizen.client;

import com.cedarsoftware.io.JsonIo;
import com.comparizen.client.model.DiffConfig;
import com.comparizen.client.model.ExcludeRegion;
import com.comparizen.client.model.TestRunStatus;
import com.comparizen.client.model.TestRunStatusResponse;
import com.comparizen.client.util.ImageType;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.matching.RequestPattern;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import uk.org.webcompere.systemstubs.environment.EnvironmentVariables;
import uk.org.webcompere.systemstubs.jupiter.SystemStubsExtension;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static com.github.tomakehurst.wiremock.stubbing.Scenario.STARTED;
import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("rawtypes")
@ExtendWith(SystemStubsExtension.class)
class ComparizenClientTest {

	private static WireMockServer wireMockServer;
	private ComparizenClient client;

	@BeforeAll
	public static void setupAll() {
		wireMockServer = new WireMockServer(options().dynamicPort());
		wireMockServer.start();
	}

	@AfterAll
	public static void teardownAll() {
		wireMockServer.stop();
	}

	@BeforeEach
	public void setup() {
		wireMockServer.resetAll();
		client = ComparizenClient.builder()
			.apiKey("some-api-key")
			.customURL("http://localhost:" + wireMockServer.port())
			.build();
	}

	@Test
	public void createTestRunWithoutName() throws ComparizenClientException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withHeader("content-type", "application/json")
				.withBody("{ \"id\": \"some-id\"}")
				.withStatus(200)));

		String testRunId = client.createTestRun("some-project-id");
		assertNotNull(testRunId);
		assertEquals("some-id", testRunId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/testrun", request1.getUrl());

		String requestBody = request1.getBodyAsString();
		Map requestMap = JsonIo.toObjects(requestBody, null, Map.class);
		assertEquals(2, requestMap.size());
		assertEquals("some-project-id", requestMap.get("projectId"));
		assertEquals("some-api-key", requestMap.get("apiKey"));
		assertNull(requestMap.get("name"));
	}

	@Test
	public void createTestRunWithName() throws ComparizenClientException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withHeader("content-type", "application/json")
				.withBody("{ \"id\": \"some-id\"}")
				.withStatus(200)));

		String testRunId = client.createTestRun("some-project-id", "some testrun name");
		assertNotNull(testRunId);
		assertEquals("some-id", testRunId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/testrun", request1.getUrl());

		String requestBody = request1.getBodyAsString();
		Map requestMap = JsonIo.toObjects(requestBody, null, Map.class);
		assertEquals(3, requestMap.size());
		assertEquals("some-project-id", requestMap.get("projectId"));
		assertEquals("some-api-key", requestMap.get("apiKey"));
		assertEquals("some testrun name", requestMap.get("name"));
	}

	@Test
	public void createTestRunWithGeneratedEnvParams(EnvironmentVariables customEnvVariables) throws ComparizenClientException {
		customEnvVariables.set("BITBUCKET_COMMIT", "some-bb-commit-id");
		customEnvVariables.set("BITBUCKET_REPO_SLUG", "some-bb-repo-slug");
		customEnvVariables.set("BITBUCKET_WORKSPACE", "some-bb-workspace");

		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withHeader("content-type", "application/json")
				.withBody("{ \"id\": \"some-id\"}")
				.withStatus(200)));

		String testRunId = client.createTestRun(
			"some-project-id",
			"some testrun name");
		assertNotNull(testRunId);
		assertEquals("some-id", testRunId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/testrun", request1.getUrl());

		String requestBody = request1.getBodyAsString();
		Map requestMap = JsonIo.toObjects(requestBody, null, Map.class);
		assertEquals(4, requestMap.size());
		assertEquals("some-project-id", requestMap.get("projectId"));
		assertEquals("some-api-key", requestMap.get("apiKey"));
		assertEquals("some testrun name", requestMap.get("name"));

		Map envProps = (Map) requestMap.get("environmentProperties");
		assertNotNull(envProps);
		assertEquals("some-bb-commit-id", envProps.get("commitId"));
		assertEquals("some-bb-repo-slug", envProps.get("bitBucketRepoSlug"));
		assertEquals("some-bb-workspace", envProps.get("bitBucketWorkspaceSlug"));
	}

	@Test
	public void createTestRunWithNameAndCustomEnvParams() throws ComparizenClientException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withHeader("content-type", "application/json")
				.withBody("{ \"id\": \"some-id\"}")
				.withStatus(200)));

		Map<String, String> envProps = new HashMap<>();
		envProps.put("some-key-1", "some-value-1");
		envProps.put("some-key-2", "some-value-2");

		String testRunId = client.createTestRun(
			"some-project-id",
			"some testrun name",
			envProps);
		assertNotNull(testRunId);
		assertEquals("some-id", testRunId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/testrun", request1.getUrl());

		String requestBody = request1.getBodyAsString();
		Map requestMap = JsonIo.toObjects(requestBody, null, Map.class);
		assertEquals(4, requestMap.size());
		assertEquals("some-project-id", requestMap.get("projectId"));
		assertEquals("some-api-key", requestMap.get("apiKey"));
		assertEquals("some testrun name", requestMap.get("name"));

		Map envPropsReq = (Map) requestMap.get("environmentProperties");
		assertNotNull(envPropsReq);
		assertEquals("some-value-1", envPropsReq.get("some-key-1"));
		assertEquals("some-value-2", envPropsReq.get("some-key-2"));
	}

	@Test
	public void createTestRunWithNameContainingDoubleQuotes() throws ComparizenClientException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withHeader("content-type", "application/json")
				.withBody("{ \"id\": \"some-id\"}")
				.withStatus(200)));

		String testRunId = client.createTestRun("some-project-id", "some \"testrun\" \"name");
		assertNotNull(testRunId);
		assertEquals("some-id", testRunId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/testrun", request1.getUrl());

		String requestBody = request1.getBodyAsString();
		Map requestMap = JsonIo.toObjects(requestBody, null, Map.class);
		assertEquals(3, requestMap.size());
		assertEquals("some-project-id", requestMap.get("projectId"));
		assertEquals("some-api-key", requestMap.get("apiKey"));
		assertEquals("some \"testrun\" \"name", requestMap.get("name"));
	}

	@Test
	public void createTestRunServerError() {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("internal server error")
				.withStatus(500)));

		ComparizenClientException exception = assertThrows(ComparizenClientException.class,
			() -> client.createTestRun("some-project-id"));

		assertEquals("Something went wrong while creating a test run. There might be a temporary problem with the Comparizen server. Please try again. If this problem persist, please contact Comparizen support. Details: status code 500 - internal server error.", exception.getMessage());
	}

	@Test
	public void createTestRunAuthenticationError() {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody((String)null)
				.withStatus(400)));

		ComparizenClientException exception = assertThrows(ComparizenClientException.class,
			() -> client.createTestRun("some-project-id"));

		assertEquals("Something went wrong while creating a test run. Make sure your API key and project ID are correct and you're using the most recent version of the Comparizen Java Client. If this problem persist, please contact Comparizen support. Details: status code 400.", exception.getMessage());
	}

	@Test
	public void createComparison() throws ComparizenClientException, URISyntaxException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("{ \"comparisonId\": \"some-comparison-id\"}")
				.withStatus(200)));

		String comparisonId = client.createComparison("some-testrun-id", "some-name",
			Paths.get(ClassLoader.getSystemResource("test-image-1.png").toURI()));
		assertEquals("some-comparison-id", comparisonId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/comparison", request1.getUrl());

		assertTrue(request1.isMultipart());
		assertEquals("some-name", request1.getPart("name").getBody().asString());
		assertEquals("some-api-key", request1.getPart("apiKey").getBody().asString());
		assertEquals("some-testrun-id", request1.getPart("testRunId").getBody().asString());
		assertEquals("", request1.getPart("tags").getBody().asString());

		Request.Part filePart = request1.getPart("file");
		assertEquals("image/png", filePart.getHeader("content-type").firstValue());
		assertTrue(filePart.getBody().asBytes().length > 9000);
	}

	@Test
	public void createComparisonBufferedImage() throws ComparizenClientException, IOException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("{ \"comparisonId\": \"some-comparison-id\"}")
				.withStatus(200)));

		String comparisonId = client.createComparison("some-testrun-id", "some-name",
			ImageIO.read(ClassLoader.getSystemResource("test-image-1.png").openStream()), ImageType.PNG);
		assertEquals("some-comparison-id", comparisonId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/comparison", request1.getUrl());

		assertTrue(request1.isMultipart());
		assertEquals("some-name", request1.getPart("name").getBody().asString());
		assertEquals("some-api-key", request1.getPart("apiKey").getBody().asString());
		assertEquals("some-testrun-id", request1.getPart("testRunId").getBody().asString());

		Request.Part filePart = request1.getPart("file");
		assertEquals("image/png", filePart.getHeader("content-type").firstValue());
		assertTrue(filePart.getBody().asBytes().length > 6000);
	}

	@Test
	public void createComparisonWithTags() throws ComparizenClientException, URISyntaxException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("{ \"comparisonId\": \"some-comparison-id\"}")
				.withStatus(200)));

		String comparisonId = client.createComparison(
			"some-testrun-id",
			"some-name",
			Paths.get(ClassLoader.getSystemResource("test-image-1.png").toURI()),
			ComparisonProperties.builder().tagNames(Arrays.asList("tag-1", "tag 2", "tag,withcomma")).build());
		assertEquals("some-comparison-id", comparisonId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/comparison", request1.getUrl());

		assertTrue(request1.isMultipart());
		assertEquals("some-name", request1.getPart("name").getBody().asString());
		assertEquals("some-api-key", request1.getPart("apiKey").getBody().asString());
		assertEquals("some-testrun-id", request1.getPart("testRunId").getBody().asString());
		assertEquals("tag-1,tag 2,tagwithcomma", request1.getPart("tags").getBody().asString());
	}

	@Test
	public void createComparisonWithExcludeRegions() throws ComparizenClientException, URISyntaxException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("{ \"comparisonId\": \"some-comparison-id\"}")
				.withStatus(200)));

		List<ExcludeRegion> excludeRegions = new java.util.ArrayList<>();
		excludeRegions.add(ExcludeRegion.builder()
			.x(10)
			.y(20)
			.width(200)
			.height(300)
			.build());
		excludeRegions.add(ExcludeRegion.builder()
			.x(11)
			.y(22)
			.width(222)
			.height(333)
			.build());

		String comparisonId = client.createComparison(
			"some-testrun-id",
			"some-name",
			Paths.get(ClassLoader.getSystemResource("test-image-1.png").toURI()),
			ComparisonProperties.builder().excludeRegions(excludeRegions).build());
		assertEquals("some-comparison-id", comparisonId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/comparison", request1.getUrl());

		assertTrue(request1.isMultipart());
		assertEquals("some-name", request1.getPart("name").getBody().asString());
		assertEquals("some-api-key", request1.getPart("apiKey").getBody().asString());
		assertEquals("some-testrun-id", request1.getPart("testRunId").getBody().asString());
		assertEquals("10 20 200 300,11 22 222 333", request1.getPart("excludeRegions").getBody().asString());
	}


	@Test
	public void createComparisonWithDiffConfig() throws ComparizenClientException, URISyntaxException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("{ \"comparisonId\": \"some-comparison-id\"}")
				.withStatus(200)));

		String comparisonId = client.createComparison(
			"some-testrun-id",
			"some-name",
			Paths.get(ClassLoader.getSystemResource("test-image-1.png").toURI()),
			ComparisonProperties.builder().diffConfig(DiffConfig.builder().perPixelThreshold(10.0).totalDiffPercentageThreshold(0.002).build()).build());
		assertEquals("some-comparison-id", comparisonId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/comparison", request1.getUrl());

		assertTrue(request1.isMultipart());
		assertEquals("some-name", request1.getPart("name").getBody().asString());
		assertEquals("some-api-key", request1.getPart("apiKey").getBody().asString());
		assertEquals("some-testrun-id", request1.getPart("testRunId").getBody().asString());
		assertEquals("10.0", request1.getPart("perPixelThreshold").getBody().asString());
		assertEquals("0.002", request1.getPart("totalDiffPercentageThreshold").getBody().asString());
	}

	@Test
	public void createComparisonWithPartialDiffConfig() throws ComparizenClientException, URISyntaxException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("{ \"comparisonId\": \"some-comparison-id\"}")
				.withStatus(200)));

		String comparisonId = client.createComparison(
			"some-testrun-id",
			"some-name",
			Paths.get(ClassLoader.getSystemResource("test-image-1.png").toURI()),
			ComparisonProperties.builder().diffConfig(DiffConfig.builder().perPixelThreshold(10.0).build()).build());
		assertEquals("some-comparison-id", comparisonId);

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/comparison", request1.getUrl());

		assertTrue(request1.isMultipart());
		assertEquals("some-name", request1.getPart("name").getBody().asString());
		assertEquals("some-api-key", request1.getPart("apiKey").getBody().asString());
		assertEquals("some-testrun-id", request1.getPart("testRunId").getBody().asString());
		assertEquals("10.0", request1.getPart("perPixelThreshold").getBody().asString());
		assertEquals(0, request1.getParts().stream().filter(p -> p.getName().equals("totalDiffPercentageThreshold")).count());
	}

	@Test
	public void finalizeTestRun() throws ComparizenClientException {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withStatus(200)));

		client.finalizeTestRun("some-testrun-id");

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/testrun/some-testrun-id/finalize", request1.getUrl());

		assertEquals("{\"apiKey\": \"some-api-key\"}", request1.getBodyAsString());
	}

	@Test
	public void finalizeTestRunServerError() {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("internal server error")
				.withStatus(500)));

		ComparizenClientException exception = assertThrows(ComparizenClientException.class,
			() -> client.finalizeTestRun("some-testrun-id"));

		assertEquals("Something went wrong while finalizing a test run. There might be a temporary problem with the Comparizen server. Please try again. If this problem persist, please contact Comparizen support. Details: status code 500 - internal server error.", exception.getMessage());
	}

	@Test
	public void uploadScreenshotServerError() {
		wireMockServer.stubFor(post(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("internal server error")
				.withStatus(500)));

		ComparizenClientException exception = assertThrows(ComparizenClientException.class,
			() -> client.createComparison("some-testrun-id", "some-name",
				Paths.get(ClassLoader.getSystemResource("test-image-1.png").toURI())));

		assertEquals("Something went wrong while uploading a screenshot. There might be a temporary problem with the Comparizen server. Please try again. If this problem persist, please contact Comparizen support. Details: status code 500 - internal server error.", exception.getMessage());
	}

	@Test
	public void getTestRunStatus() throws ComparizenClientException {
		wireMockServer.stubFor(get(urlMatching(".*"))
			.willReturn(aResponse()
				.withHeader("content-type", "application/json")
				.withBody("{ \"url\": \"some-url\", \"status\": \"PROCESSING\" }")
				.withStatus(200)));

		TestRunStatusResponse response = client.getTestRunStatus("some-testrun-id");
		assertEquals(TestRunStatus.PROCESSING, response.getTestRunStatus());
		assertEquals("some-url", response.getUrl());

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(1, receivedRequests.size());
		LoggedRequest request1 = receivedRequests.get(0);
		assertEquals("/rest/testrun/some-testrun-id/status?apiKey=some-api-key", request1.getUrl());
	}

	@Test
	public void getTestRunStatusUnknownStatus() throws ComparizenClientException {
		wireMockServer.stubFor(get(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("{ \"url\": \"some-url\", \"status\": \"SOMETHING NOT SUPPORTED\" }")
				.withStatus(200)));

		TestRunStatusResponse response = client.getTestRunStatus("some-testrun-id");
		assertEquals(TestRunStatus.UNKNOWN, response.getTestRunStatus());
		assertEquals("some-url", response.getUrl());
	}

	@Test
	public void getTestRunStatusServerError() {
		wireMockServer.stubFor(get(urlMatching(".*"))
			.willReturn(aResponse()
				.withBody("internal server error")
				.withStatus(500)));

		ComparizenClientException exception = assertThrows(ComparizenClientException.class,
			() -> client.getTestRunStatus("some-testrun-id"));

		assertEquals("Something went wrong while retrieving test run status. There might be a temporary problem with the Comparizen server. Please try again. If this problem persist, please contact Comparizen support. Details: status code 500 - internal server error.", exception.getMessage());
	}

	@Test
	public void waitUntilTestRunResult() throws InterruptedException, ComparizenClientException {
		wireMockServer.stubFor(get(urlMatching(".*"))
			.inScenario("waitfor")
			.whenScenarioStateIs(STARTED)
			.willReturn(aResponse()
				.withBody("{ \"url\": \"some-url1\", \"status\": \"PROCESSING\" }")
				.withStatus(200))
			.willSetStateTo("processing-2"));

		wireMockServer.stubFor(get(urlMatching(".*"))
			.inScenario("waitfor")
			.whenScenarioStateIs("processing-2")
			.willReturn(aResponse()
				.withBody("{ \"url\": \"some-url2\", \"status\": \"PROCESSING\" }")
				.withStatus(200))
			.willSetStateTo("processing-3"));

		wireMockServer.stubFor(get(urlMatching(".*"))
			.inScenario("waitfor")
			.whenScenarioStateIs("processing-3")
			.willReturn(aResponse()
				.withBody("{ \"url\": \"some-url3\", \"status\": \"DIFFS_FOUND\" }")
				.withStatus(200)));

		TestRunStatusResponse result = client.waitUntilTestRunResult("some-testrun-id", 5000);
		assertEquals(TestRunStatus.DIFFS_FOUND, result.getTestRunStatus());
		assertEquals("some-url3", result.getUrl());

		List<LoggedRequest> receivedRequests = wireMockServer
			.findRequestsMatching(RequestPattern.everything())
			.getRequests();
		assertEquals(3, receivedRequests.size());
	}

	@Test
	public void waitUntilTestRunResultTimedOut() {
		wireMockServer.stubFor(get(urlMatching(".*"))
			.inScenario("waitfor")
			.whenScenarioStateIs(STARTED)
			.willReturn(aResponse()
				.withBody("{ \"url\": \"some-url\", \"status\": \"PROCESSING\" }")
				.withStatus(200)));

		ComparizenClientException exception = assertThrows(ComparizenClientException.class,
			() -> client.waitUntilTestRunResult("some-testrun-id", 1000));

		assertEquals("Test run with id some-testrun-id was still processing after 1000 ms passed.", exception.getMessage());
	}

	@Test
	public void waitUntilTestRunResultServerError() {
		wireMockServer.stubFor(get(urlMatching(".*"))
			.inScenario("waitfor")
			.whenScenarioStateIs(STARTED)
			.willReturn(aResponse()
				.withBody("{ \"url\": \"some-url\", \"status\": \"PROCESSING\" }")
				.withStatus(200))
			.willSetStateTo("processing-2"));

		wireMockServer.stubFor(get(urlMatching(".*"))
			.inScenario("waitfor")
			.whenScenarioStateIs("processing-2")
			.willReturn(aResponse()
				.withBody("some error")
				.withStatus(500))
			.willSetStateTo("processing-3"));

		ComparizenClientException exception = assertThrows(ComparizenClientException.class,
			() -> client.waitUntilTestRunResult("some-testrun-id", 5000));

		assertEquals("Something went wrong while retrieving test run status. There might be a temporary problem with the Comparizen server. Please try again. If this problem persist, please contact Comparizen support. Details: status code 500 - some error.", exception.getMessage());
	}


}
