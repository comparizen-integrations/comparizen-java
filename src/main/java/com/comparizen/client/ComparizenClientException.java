package com.comparizen.client;

@SuppressWarnings("unused")
public class ComparizenClientException extends Exception {
    public ComparizenClientException() {
    }

    public ComparizenClientException(String message) {
        super(message);
    }

    public ComparizenClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
