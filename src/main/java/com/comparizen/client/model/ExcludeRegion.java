package com.comparizen.client.model;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class ExcludeRegion {
	int x;
	int y;
	int width;
	int height;
}
