package integrationtests;

import com.comparizen.client.ComparisonProperties;
import com.comparizen.client.ComparizenClient;
import com.comparizen.client.ComparizenClientException;
import com.comparizen.client.model.DiffConfig;
import com.comparizen.client.model.ExcludeRegion;
import com.comparizen.client.model.TestRunStatus;
import com.comparizen.client.model.TestRunStatusResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ComparizenClientIT {

	private static final String PROJECT_ID = "bbd57388-dcc3-46f3-b443-1c8b6a436b9f";

	private ComparizenClient client;

	@BeforeEach
	public void setup() {
		client = ComparizenClient.builder()
			.apiKey(System.getenv("COMPARIZEN_TEST_APIKEY"))
			.build();
	}

	@Test
	public void test() throws ComparizenClientException, URISyntaxException, InterruptedException {
		String testRunId = client.createTestRun(PROJECT_ID, "my testrun name");
		assertNotNull(testRunId);

		String screenshotId1 = client.createComparison(testRunId, "some screenshot name 1", Paths.get(ClassLoader.getSystemResource("test-image-1.png").toURI()));
		String screenshotId2 = client.createComparison(testRunId, "some screenshot name 2",
			Paths.get(ClassLoader.getSystemResource("test-image-2.png").toURI()),
			ComparisonProperties.builder().tagNames(Arrays.asList("tag-1", "tag 2")).build());

		assertNotNull(screenshotId1);
		assertNotNull(screenshotId2);
		assertNotEquals(screenshotId1, screenshotId2);

		TestRunStatusResponse response = client.getTestRunStatus(testRunId);
		assertTrue(response.getTestRunStatus() == TestRunStatus.PROCESSING || response.getTestRunStatus() == TestRunStatus.DIFFS_FOUND);
		assertEquals(response.getUrl(), "https://app.comparizen.com/#/p/" + PROJECT_ID + "/tr/" + testRunId);
		client.finalizeTestRun(testRunId);

		TestRunStatusResponse endStatusResponse = client.waitUntilTestRunResult(testRunId, 30000L);
		assertEquals(TestRunStatus.DIFFS_FOUND, endStatusResponse.getTestRunStatus());
		assertEquals("https://app.comparizen.com/#/p/" + PROJECT_ID + "/tr/" + testRunId, endStatusResponse.getUrl());
	}

	@Test
	public void testWithThreshold() throws ComparizenClientException, URISyntaxException, InterruptedException {
		String testRunId = client.createTestRun(PROJECT_ID, "my testrun name");
		assertNotNull(testRunId);

		// make sure the comparizen project has a screenshot with this name in the baseline, using image 1
		String screenshotId1 = client.createComparison(testRunId, "some screenshot name with baseline",
			Paths.get(ClassLoader.getSystemResource("test-image-2.png").toURI()),
			ComparisonProperties.builder().diffConfig(DiffConfig.builder().perPixelThreshold(97.0).build()).build()
		);

		assertNotNull(screenshotId1);

		TestRunStatusResponse response = client.getTestRunStatus(testRunId);
		assertTrue(response.getTestRunStatus() == TestRunStatus.PROCESSING || response.getTestRunStatus() == TestRunStatus.DIFFS_FOUND);
		assertEquals(response.getUrl(), "https://app.comparizen.com/#/p/" + PROJECT_ID + "/tr/" + testRunId);
		client.finalizeTestRun(testRunId);

		TestRunStatusResponse endStatusResponse = client.waitUntilTestRunResult(testRunId, 30000L);
		assertEquals(TestRunStatus.ALL_OK, endStatusResponse.getTestRunStatus());
		assertEquals("https://app.comparizen.com/#/p/" + PROJECT_ID + "/tr/" + testRunId, endStatusResponse.getUrl());
	}

	@Test
	public void testWithThreshold2() throws ComparizenClientException, URISyntaxException, InterruptedException {
		String testRunId = client.createTestRun(PROJECT_ID, "my testrun name");
		assertNotNull(testRunId);

		// make sure the comparizen project has a screenshot with this name in the baseline, using image 1
		String screenshotId1 = client.createComparison(testRunId, "some screenshot name with baseline",
			Paths.get(ClassLoader.getSystemResource("test-image-2.png").toURI()),
			ComparisonProperties.builder().diffConfig(DiffConfig.builder().totalDiffPercentageThreshold(80.0).build()).build()
		);

		assertNotNull(screenshotId1);

		TestRunStatusResponse response = client.getTestRunStatus(testRunId);
		assertTrue(response.getTestRunStatus() == TestRunStatus.PROCESSING || response.getTestRunStatus() == TestRunStatus.DIFFS_FOUND);
		assertEquals("https://app.comparizen.com/#/p/" + PROJECT_ID + "/tr/" + testRunId, response.getUrl());
		client.finalizeTestRun(testRunId);

		TestRunStatusResponse endStatusResponse = client.waitUntilTestRunResult(testRunId, 30000L);
		assertEquals(TestRunStatus.ALL_OK, endStatusResponse.getTestRunStatus());
		assertEquals("https://app.comparizen.com/#/p/" + PROJECT_ID + "/tr/" + testRunId, endStatusResponse.getUrl());
	}

	@Test
	public void testWithExcludeRegions() throws ComparizenClientException, URISyntaxException, InterruptedException {
		String testRunId = client.createTestRun(PROJECT_ID, "my testrun name");
		assertNotNull(testRunId);

		// make sure the comparizen project has a screenshot with this name in the baseline, using image 1
		List<ExcludeRegion> excludeRegions = new java.util.ArrayList<>();
		excludeRegions.add(ExcludeRegion.builder()
			.x(10)
			.y(20)
			.width(200)
			.height(300)
			.build());
		String screenshotId1 = client.createComparison(testRunId, "some screenshot name with exclude regions",
			Paths.get(ClassLoader.getSystemResource("test-image-2.png").toURI()),
			ComparisonProperties.builder()
				.excludeRegions(excludeRegions)
				.build()
		);

		assertNotNull(screenshotId1);

		TestRunStatusResponse response = client.getTestRunStatus(testRunId);
		assertTrue(response.getTestRunStatus() == TestRunStatus.PROCESSING || response.getTestRunStatus() == TestRunStatus.DIFFS_FOUND);
		assertEquals(response.getUrl(), "https://app.comparizen.com/#/p/" + PROJECT_ID + "/tr/" + testRunId);
		client.finalizeTestRun(testRunId);

		TestRunStatusResponse endStatusResponse = client.waitUntilTestRunResult(testRunId, 30000L);
		assertEquals(TestRunStatus.DIFFS_FOUND, endStatusResponse.getTestRunStatus());
		assertEquals("https://app.comparizen.com/#/p/" + PROJECT_ID + "/tr/" + testRunId, endStatusResponse.getUrl());
	}
}
