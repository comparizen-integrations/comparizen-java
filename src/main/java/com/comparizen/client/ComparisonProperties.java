package com.comparizen.client;

import com.comparizen.client.model.DiffConfig;
import com.comparizen.client.model.ExcludeRegion;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class ComparisonProperties {
	private List<String> tagNames;
	private List<ExcludeRegion> excludeRegions;
	private DiffConfig diffConfig;
}
