# Comparizen Java Client example

This directory contains a simple example of a fully configured Java Gradle project using the [Comparizen Java client library](https://gitlab.com/comparizen-integrations/comparizen-java).

For more details on what Comparizen is and how to use it, consult the documentation [here](https://app.comparizen.com/docs/).

## Getting started

1. Make sure you have Java 17 or higher installed. You can download and install a JDK [here](https://adoptium.net/).
2. If there's no `.env` file present in this directory yet: 
   1. make a copy of `.env-template` and rename it to `.env`
   2. Enter your Comparizen project's ID and API KEY in the `.env` file. To find these, open your project in Comparizen and click on the settings icon next to the project name.
3. Import the project in your favorite IDE or run `./gradlew test` from a terminal to run a test that will upload screenshots to your Comparizen project.
   - Note that the test will always fail the first time, as you'll need to accept any new uploaded screenshot to your Comparizen project's baseline.
4. See the example test code in `src/test/java/example/ExampleTest.java`

