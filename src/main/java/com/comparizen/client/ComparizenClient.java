package com.comparizen.client;

import com.cedarsoftware.io.JsonIo;
import com.comparizen.client.model.DiffConfig;
import com.comparizen.client.model.TestRunStatus;
import com.comparizen.client.model.TestRunStatusResponse;
import com.comparizen.client.util.HttpResponse;
import com.comparizen.client.util.HttpUtil;
import com.comparizen.client.util.ImageType;
import com.comparizen.client.util.UnknownMimeTypeException;
import lombok.Builder;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class ComparizenClient {

	private static final String INVALID_JSON_ERROR_MESSAGE = "Received unexpected response from Comparizen server. Check your internet connection. " +
		"If this problem persist, see the Comparizen website if there's a newer version of this client.";

	@SuppressWarnings("FieldCanBeLocal")
	private final String DEFAULT_URL = "https://app.comparizen.com";

	private final String url;
	private final String apiKey;

	@Builder
	private ComparizenClient(String apiKey, String customURL) {
		this.apiKey = apiKey;
		this.url = customURL == null ? DEFAULT_URL : customURL;
	}

	/**
	 * Creates a new Comparizen test run
	 *
	 * @param projectId the project's ID where the test run should be created.
	 * @return the new test run's ID
	 * @throws ComparizenClientException when the test run could not be created
	 */
	public String createTestRun(String projectId) throws ComparizenClientException {
		return this.createTestRun(projectId, null);
	}

	/**
	 * Creates a new Comparizen test run
	 *
	 * @param projectId the project's ID where the test run should be created.
	 * @param name      a name describing this test run (optional)
	 * @return the new test run's ID
	 * @throws ComparizenClientException when the test run could not be created
	 */
	public String createTestRun(String projectId, String name) throws ComparizenClientException {
		return this.createTestRun(projectId, name, null);
	}

	/**
	 * Creates a new Comparizen test run
	 *
	 * @param projectId                   the project's ID where the test run should be created.
	 * @param name                        a name describing this test run (optional)
	 * @param customEnvironmentProperties custom environment properties,
	 *                                    used for integrating Comparizen test results with other systems, like CI-builds.
	 * @return the new test run's ID
	 * @throws ComparizenClientException when the test run could not be created
	 */
	public String createTestRun(String projectId, String name, final Map<String, String> customEnvironmentProperties) throws ComparizenClientException {
		String action = "creating a test run";

		Map<String, String> environmentProperties = customEnvironmentProperties == null ? generateEnvironmentProperties() : customEnvironmentProperties;
		Map<String, Object> testRunProps = new HashMap<>();
		testRunProps.put("apiKey", apiKey);
		testRunProps.put("projectId", projectId);

		if (name != null) {
			testRunProps.put("name", name);
		}

		if (!environmentProperties.isEmpty()) {
			testRunProps.put("environmentProperties", environmentProperties);
		}

		try {
			HttpResponse response = HttpUtil.doPost(url + "/rest/testrun",
				JsonIo.toJson(testRunProps, null));

			verifyResponse(action, response);
			return getStringFromResponse(response, "id");
		} catch (final IOException e) {
			throw generateConnectionErrorException(action, e);
		}
	}

	/**
	 * Finalizes a Comparizen test run. Finalizing a test run marks the test run as 'completed' and
	 * will prevent the test run from accepting new screenshots.
	 *
	 * @param testRunId the test run's ID
	 * @throws ComparizenClientException when the test run could not be finalized
	 */
	public void finalizeTestRun(String testRunId) throws ComparizenClientException {
		String action = "finalizing a test run";
		try {
			HttpResponse response = HttpUtil.doPost(
				url + "/rest/testrun/" + testRunId + "/finalize",
				"{" +
					"\"apiKey\": \"" + apiKey + "\"" +
					"}");

			verifyResponse(action, response);
		} catch (final IOException e) {
			throw generateConnectionErrorException(action, e);
		}
	}

	/**
	 * Adds a new comparison (screenshot) to a test run
	 *
	 * @param testRunId the test run's ID
	 * @param name      the name of the screenshot. This screenshot will be compared to a screenshot from the testrun project's baseline with the same name.
	 * @param file      path to the screenshot file
	 * @return the ID of the newly created comparison
	 * @throws ComparizenClientException when something went wrong while uploading the screenshot or creating the comparison
	 */
	public String createComparison(String testRunId, String name, Path file) throws ComparizenClientException {
		return performUpload(testRunId, name, file, null, null);
	}

	/**
	 * Adds a new comparison (screenshot) to a test run
	 *
	 * @param testRunId     the test run's ID
	 * @param name          the name of the screenshot. This screenshot will be compared to a screenshot from the testrun project's baseline with the same name.
	 * @param bufferedImage A BufferedImage containing image data
	 * @param imageType     an ImageType.
	 * @return the ID of the newly created comparison
	 * @throws ComparizenClientException when something went wrong while uploading the screenshot or creating the comparison
	 */
	public String createComparison(String testRunId, String name, BufferedImage bufferedImage, ImageType imageType) throws ComparizenClientException {
		return performUpload(testRunId, name, bufferedImage, imageType, null);
	}

	/**
	 * Adds a new comparison (screenshot) to a test run
	 *
	 * @param testRunId  the test run's ID
	 * @param name       the name of the screenshot. This screenshot will be compared to a screenshot from the testrun project's baseline with the same name.
	 * @param file       path to the screenshot file
	 * @param properties additional properties for this comparison, such as tag names.
	 * @return the ID of the newly created comparison
	 * @throws ComparizenClientException when something went wrong while uploading the screenshot or creating the comparison
	 */
	public String createComparison(String testRunId, String name, Path file, ComparisonProperties properties) throws ComparizenClientException {
		return performUpload(testRunId, name, file, null, properties);
	}

	/**
	 * Adds a new comparison (screenshot) to a test run
	 *
	 * @param testRunId     the test run's ID
	 * @param name          the name of the screenshot. This screenshot will be compared to a screenshot from the testrun project's baseline with the same name.
	 * @param bufferedImage A BufferedImage containing image data
	 * @param imageType     an ImageType.
	 * @param properties    additional properties for this comparison, such as tag names.
	 * @return the ID of the newly created comparison
	 * @throws ComparizenClientException when something went wrong while uploading the screenshot or creating the comparison
	 */
	public String createComparison(String testRunId, String name, BufferedImage bufferedImage, ImageType imageType, ComparisonProperties properties) throws ComparizenClientException {
		return performUpload(testRunId, name, bufferedImage, imageType, properties);
	}

	private String performUpload(String testRunId, String name, Object bufferedImageOrPath, ImageType forcedImageType, ComparisonProperties properties) throws ComparizenClientException {
		String action = "uploading a screenshot";

		if (!(bufferedImageOrPath instanceof BufferedImage) && !(bufferedImageOrPath instanceof Path)) {
			throw new ComparizenClientException("Screenshot upload can only be done using a Path or BufferedImage object");
		}

		Map<String, Object> postParams = new HashMap<>();
		postParams.put("apiKey", apiKey);
		postParams.put("testRunId", testRunId);
		postParams.put("name", name);
		postParams.put("tags", extractCommaSeparatedTags(properties));
		postParams.put("excludeRegions", extractExcludeRegions(properties));
		postParams.put("file", bufferedImageOrPath);

		if (properties != null && properties.getDiffConfig() != null) {
			DiffConfig diffConfig = properties.getDiffConfig();
			if (diffConfig.getPerPixelThreshold() != null) {
				postParams.put("perPixelThreshold", diffConfig.getPerPixelThreshold());
			}

			if (diffConfig.getTotalDiffPercentageThreshold() != null) {
				postParams.put("totalDiffPercentageThreshold", diffConfig.getTotalDiffPercentageThreshold());
			}
		}

		try {
			HttpResponse response = HttpUtil.postMultipart(url + "/rest/comparison", postParams, forcedImageType);
			int responseStatusCode = response.getStatusCode();
			verifyResponse(action, response);

			return getStringFromResponse(response, "comparisonId");
		} catch (final IOException e) {
			throw generateConnectionErrorException(action, e);
		} catch (final UnknownMimeTypeException e) {
			throw new ComparizenClientException("Something went wrong while " + action + ": " +
				"an invalid or unknown mime type was attached to the upload content: " + e.getMessage(), e);
		}
	}

	/**
	 * Retrieves a test run's status
	 *
	 * @param testRunId the test run's ID
	 * @return a TestRunStatusResponse describing the test run's status
	 * @throws ComparizenClientException when something went wrong while retrieving the test run's status
	 */
	public TestRunStatusResponse getTestRunStatus(final String testRunId) throws ComparizenClientException {
		String action = "retrieving test run status";

		try {
			HttpResponse response = HttpUtil.doGet(url + "/rest/testrun/" + testRunId + "/status?apiKey=" + this.apiKey);
			int responseStatusCode = response.getStatusCode();
			verifyResponse(action, response);

			@SuppressWarnings("rawtypes") final Map responseMap = JsonIo.toObjects(response.getBody(), null, Map.class);
			if (responseMap == null || !responseMap.containsKey("status") || !responseMap.containsKey("url")) {
				throw new ComparizenClientException(INVALID_JSON_ERROR_MESSAGE);
			}

			return TestRunStatusResponse.builder()
				.testRunStatus(TestRunStatus.fromString(String.valueOf(responseMap.get("status"))))
				.url(String.valueOf(responseMap.get("url")))
				.build();
		} catch (final IOException e) {
			throw generateConnectionErrorException(action, e);
		}
	}

	/**
	 * Waits until a test run is either finalized or the given timeout is passed.
	 * This method will poll the Comparizen service every 500 ms.
	 *
	 * @param testRunId a test run's ID
	 * @param timeoutMs the maximum amount of milliseconds to wait.
	 * @return a TestRunStatusResponse
	 * @throws ComparizenClientException when timeout exceeded or something went wrong while retrieving the test run status
	 * @throws InterruptedException      when the current Thread is interrupted
	 */
	@SuppressWarnings("BusyWait")
	public TestRunStatusResponse waitUntilTestRunResult(final String testRunId, final long timeoutMs) throws ComparizenClientException, InterruptedException {
		long startTime = System.currentTimeMillis();

		while (startTime + timeoutMs >= System.currentTimeMillis()) {
			TestRunStatusResponse response = getTestRunStatus(testRunId);

			if (response.getTestRunStatus() != TestRunStatus.PROCESSING) {
				return response;
			}
			Thread.sleep(500);
		}

		throw new ComparizenClientException("Test run with id " + testRunId + " was still processing after " + timeoutMs + " ms passed.");
	}

	private Map<String, String> generateEnvironmentProperties() {
		Map<String, String> env = System.getenv();
		if (env.containsKey("BITBUCKET_COMMIT")) {
			final Map<String, String> envMap = new HashMap<>();
			envMap.put("commitId", env.getOrDefault("BITBUCKET_COMMIT", null));
			envMap.put("bitBucketRepoSlug", env.getOrDefault("BITBUCKET_REPO_SLUG", null));
			envMap.put("bitBucketWorkspaceSlug", env.getOrDefault("BITBUCKET_WORKSPACE", null));
			return envMap;
		}

		return Collections.emptyMap();
	}

	private String extractCommaSeparatedTags(final ComparisonProperties props) {
		if (props == null || props.getTagNames() == null) {
			return "";
		}

		return props.getTagNames().stream()
			.map(t -> t.replaceAll(",", ""))
			.collect(Collectors.joining(","));
	}

	private String extractExcludeRegions (final ComparisonProperties props) {
		if (props == null || props.getExcludeRegions() == null) {
			return "";
		}

		return props.getExcludeRegions().stream()
			.map(er -> String.format("%s %s %s %s", er.getX(), er.getY(), er.getWidth(), er.getHeight()))
			.collect(Collectors.joining(","));
	}

	@SuppressWarnings("rawtypes")
	private String getStringFromResponse(final HttpResponse response, final String fieldKey) throws ComparizenClientException {
		try {
			final Map responseMap = JsonIo.toObjects(response.getBody(), null, Map.class);
			if (responseMap == null || !responseMap.containsKey(fieldKey)) {
				throw new ComparizenClientException(INVALID_JSON_ERROR_MESSAGE);
			}

			return String.valueOf(responseMap.get(fieldKey));
		} catch (Exception e) {
			throw new ComparizenClientException(INVALID_JSON_ERROR_MESSAGE);
		}
	}

	private void verifyResponse(final String action, final HttpResponse response) throws ComparizenClientException {
		int statusCode = response.getStatusCode();
		if (statusCode < 400) {
			return;
		}

		List<String> messageLines = new ArrayList<>();
		messageLines.add("Something went wrong while " + action + ".");

		String explainerText = "";
		if (statusCode == 400 || statusCode == 401 || statusCode == 403) {
			messageLines.add("Make sure your API key and project ID are correct and you're using the most recent version of the Comparizen Java Client. If this problem persist, please contact Comparizen support.");
		} else if (statusCode == 500) {
			messageLines.add("There might be a temporary problem with the Comparizen server. Please try again. If this problem persist, please contact Comparizen support.");
		}


		String statusCodeMessage = "Details: status code " + statusCode;
		String responseBody = response.getBody();
		if (responseBody != null && !responseBody.isEmpty()) {
			messageLines.add(statusCodeMessage + " - " + responseBody + ".");
		} else {
			messageLines.add(statusCodeMessage + ".");
		}

		throw new ComparizenClientException(String.join(" ", messageLines));
	}

	private ComparizenClientException generateConnectionErrorException(final String action, final Exception cause) {
		return new ComparizenClientException("Something went wrong while " + action + ": " +
			"an exception occurred while communicating with the Comparizen server: " + cause.getMessage(), cause);
	}


}
