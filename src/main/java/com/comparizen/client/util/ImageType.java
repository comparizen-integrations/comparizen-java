package com.comparizen.client.util;

public enum ImageType {
    PNG("image/png", "png");

    private final String mimeType;
    private final String imageIoType;

    ImageType(String mimeType, String imageIoType) {
        this.mimeType = mimeType;
        this.imageIoType = imageIoType;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getImageIoType() {
        return imageIoType;
    }
}
