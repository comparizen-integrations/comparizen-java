package com.comparizen.client.util;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class HttpResponse {
	int statusCode;
	String body;
}
