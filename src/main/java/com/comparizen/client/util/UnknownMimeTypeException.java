package com.comparizen.client.util;

public class UnknownMimeTypeException extends Exception {

    public UnknownMimeTypeException() {
        super();
    }

    public UnknownMimeTypeException(String message) {
        super(message);
    }
}
