package example;

import com.comparizen.client.ComparisonProperties;
import com.comparizen.client.ComparizenClient;
import com.comparizen.client.ComparizenClientException;
import com.comparizen.client.model.TestRunStatus;
import com.comparizen.client.model.TestRunStatusResponse;
import io.github.cdimascio.dotenv.Dotenv;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExampleTest {

	private static final Dotenv dotenv = Dotenv.load();
	private static final String PROJECT_ID = dotenv.get("COMPARIZEN_PROJECT_ID");


	private ComparizenClient client;

	@BeforeEach
	public void setup() {
		Dotenv dotenv = Dotenv.load();
		// before running this test, make sure there's a `.env` file in this project's directory containing your Comparizen's project ID and api key. See the README.md for details.
		client = ComparizenClient.builder()
			.apiKey(dotenv.get("COMPARIZEN_API_KEY"))
			.build();
	}

	@Test
	public void test() throws ComparizenClientException, URISyntaxException, InterruptedException {
		String testRunId = client.createTestRun(PROJECT_ID, "my testrun name");

		// uses a test image file from src/java/resources
		client.createComparison(testRunId, "my screenshot 1", getImagePath("test-image-1.png"));
		client.createComparison(testRunId, "my screenshot 2", getImagePath("test-image-2.png"),
			ComparisonProperties.builder()
				.tagNames(List.of("my tag 1", "my tag 2"))
				.build());

		client.finalizeTestRun(testRunId);

		TestRunStatusResponse response = client.waitUntilTestRunResult(testRunId, 10000L);

		// if this test fails, open the URL in the message to review the difference(s) and/or accept the new image to your project's baseline.
		assertEquals(TestRunStatus.ALL_OK, response.getTestRunStatus(), "Comparizen reported a difference between screenshot and baseline. See " + response.getUrl() + " for more details.");
	}

	private static Path getImagePath(String name) throws URISyntaxException {
		return Paths.get(ClassLoader.getSystemResource(name).toURI());
	}
}
