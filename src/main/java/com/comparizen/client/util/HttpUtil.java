package com.comparizen.client.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

public class HttpUtil {

	public static HttpResponse doGet(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setUseCaches(false);

		try {
			return HttpResponse.builder()
				.statusCode(con.getResponseCode())
				.body(readBody(con.getInputStream()).orElse(""))
				.build();
		} catch (Exception e) {
			return HttpResponse.builder()
				.statusCode(con.getResponseCode())
				.body(readBody(con.getErrorStream()).orElse(""))
				.build();
		} finally {
			con.disconnect();
		}
	}

	public static HttpResponse doPost(String urlString, String postBody) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json; utf-8");
		con.setRequestProperty("Accept", "application/json");
		con.setUseCaches(false);
		con.setDoOutput(true);

		try (OutputStream os = con.getOutputStream()) {
			byte[] input = postBody.getBytes(StandardCharsets.UTF_8);
			os.write(input, 0, input.length);
		}

		try {
			return HttpResponse.builder()
				.statusCode(con.getResponseCode())
				.body(readBody(con.getInputStream()).orElse(""))
				.build();
		} catch (Exception e) {
			return HttpResponse.builder()
				.statusCode(con.getResponseCode())
				.body(readBody(con.getErrorStream()).orElse(""))
				.build();
		} finally {
			con.disconnect();
		}
	}

	public static HttpResponse postMultipart(String urlString, Map<String, Object> postParams, ImageType forcedImageType) throws IOException, UnknownMimeTypeException {

		URL url = new URL(urlString);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		String boundary = "===imagecompare-upload-boundary-" + new BigInteger(256, new Random());
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		con.setRequestProperty("content-transfer-encoding", "binary");
		con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);


		try {
			String fieldSeparator = ("--" + boundary + "\r\nContent-Disposition: form-data; name=");
			DataOutputStream request = new DataOutputStream(con.getOutputStream());
			for (Map.Entry<String, Object> entry : postParams.entrySet()) {
				request.writeBytes(fieldSeparator);

				if (entry.getValue() instanceof Path) {
					Path path = (Path) entry.getValue();
					String mimeType = forcedImageType != null ? forcedImageType.getMimeType() : Optional.ofNullable(Files.probeContentType(path)).orElseThrow(UnknownMimeTypeException::new);
					request.writeBytes(("\"" + entry.getKey() + "\"; filename=\"" + path.getFileName()
						+ "\"\r\nContent-Type: " + mimeType + "\r\n\r\n"));
					request.write(Files.readAllBytes(path));
					request.writeBytes("\r\n");
				}
				if (entry.getValue() instanceof BufferedImage) {
					if (forcedImageType == null) {
						throw new UnknownMimeTypeException("Missing 'imageType' for BufferedImage. When using BufferedImage type, you must specifiy an ImageType as well.");
					}
					BufferedImage image = (BufferedImage) entry.getValue();
					String fileName = new BigInteger(256, new Random()).toString();

					request.write(("\"" + entry.getKey() + "\"; filename=\"" + fileName
						+ "\"\r\nContent-Type: " + forcedImageType.getMimeType() + "\r\n\r\n").getBytes(StandardCharsets.UTF_8));

					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ImageIO.write(image, forcedImageType.getImageIoType(), baos);

					baos.flush();
					request.write(baos.toByteArray());
					baos.close();

					request.write("\r\n".getBytes(StandardCharsets.UTF_8));
				} else {
					request.write(("\"" + entry.getKey() + "\"\r\n\r\n" + entry.getValue() + "\r\n")
						.getBytes(StandardCharsets.UTF_8));
				}
			}
			request.write(("--" + boundary + "--").getBytes(StandardCharsets.UTF_8));

			request.flush();

			return HttpResponse.builder()
				.statusCode(con.getResponseCode())
				.body(readBody(con.getInputStream()).orElse(""))
				.build();
		} catch (IOException e) {
			return HttpResponse.builder()
				.statusCode(con.getResponseCode())
				.body(readBody(con.getErrorStream()).orElse(""))
				.build();
		} finally {
			con.disconnect();
		}
	}

	private static Optional<String> readBody(InputStream inputStream) throws IOException {
		if (inputStream == null) {
			return Optional.empty();
		}
		try (BufferedReader br = new BufferedReader(
			new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
			StringBuilder response = new StringBuilder();
			String responseLine;
			while ((responseLine = br.readLine()) != null) {
				response.append(responseLine.trim());
			}
			return Optional.of(response.toString());
		}
	}

}
