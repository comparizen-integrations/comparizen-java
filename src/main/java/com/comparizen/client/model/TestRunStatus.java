package com.comparizen.client.model;

public enum TestRunStatus {
    ALL_OK, DIFFS_FOUND, HAS_REJECTED, PROCESSING, ERROR, UNKNOWN;

    public static TestRunStatus fromString(String string) {
        try {
            return TestRunStatus.valueOf(string);
        } catch (final IllegalArgumentException e) {
            return UNKNOWN;
        }
    }
}
