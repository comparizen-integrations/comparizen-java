# publishing new versions

1. Make sure you have a `gradle.properties` file in the project's directory, containing the signing info and credentials required for maven publish.
   (see this project's `gradle.properties.example` for an example) 
2. Create a new release branch from master: `release/<new version>` (example: `release/0.1.1`)
3. In that branch, update `build.gradle`'s `version` field to the new version, without `-SNAPSHOT` behind it.
4. Commit and push the changes in the branch. 
5. Run `gradle publish`
6. Go to https://oss.sonatype.org/index.html and open 'staging repositories'
7. Select the most recently created staging repository. If there are no errors, select it and click 'close'
8. Sonatype repo will now perform certain checks on the published resources. When that's done, click 'Release'
9. Check out the `master` branch again and upgrade the `build.gradle` file with a higher version and make sure to add the `-SNAPSHOT` postfix

After the updated JAR has reached the maven repositories (this could take 24 hours):
1. Update the gradle dependency version in the README
2. Update the comparizen-client version in the example project's dependencies
   1. re-generate the client example zip
   2. re-upload it to the spaces bucket/examples
